# Bill.com Auto Sync

This repository includes a [AutoHotKey](https://www.autohotkey.com/) script that automates the process of automatically syncing each sync profile. It supports multiple profiles, and will sync each profile one at a time in order.

### Download & Usage

Included is a compiled version inside the /bin folder an .exe file that will act as the compiled script. You may use the uncompiled .ahk file in the /src folder once you have installed [AutoHotKey](https://www.autohotkey.com/).

The script includes a menu that allows you to specify a [Slack](https://www.slack.com/) WebHook URL for error handling, It's optional. The script also includes a dropdown to let you specify how many hours to wait to run for each sync. When you change the time frame, it the next sync is when the time frame takes effect, unless you restart the script.
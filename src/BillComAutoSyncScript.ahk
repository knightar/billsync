;Setup Setting Variables
SlackUrl := ""
LoopTimerTime = 3600000

Menu, Tray, Icon, Ampeross-Qetto-2-Sync.ico
; Create the sub-menus for the menu bar:
Menu, FileMenu, Add, E&xit, FileExit
Menu, HelpMenu, Add, &About, HelpAbout

; Create the menu bar by attaching the sub-menus to it:
Menu, MyMenuBar, Add, &File, :FileMenu
Menu, MyMenuBar, Add, &Help, :HelpMenu

; Attach the menu bar to the window:
Gui, Menu, MyMenuBar

; Create the main Edit control and display the window:
Gui, +Resize  ; Make the window resizable.
;Gui, Add, Edit, vMainEdit WantTab W600 R20

Gui, Add, Text, section, How long between each sync (in hours)
Gui, Add, DropDownList, Choose1 vSelectedItem ys gChosens, 1|2|4|6|12|24
timechoice_TT := "How long between each sync (in hours)"
IfExist, billautosyncer.ini
{
	IniRead, Choice, billautosyncer.ini, Bill, delay, 1
	GuiControl, ChooseString, SelectedItem, %Choice%

	LoopTimerTime:=(Choice*60*60*1000)
}

Gui, Add, Text, section xm, Slack URL for Error Notification
Gui, Add, Edit, vSlackURL W300 ys gSlackUrlChoose

IfExist, billautosyncer.ini
{
	IniRead, SlackURLText, billautosyncer.ini, Bill, SlackURLSetting, %A_Space%
	global SlackURL
	SlackURL := SlackURLText
	GuiControl, Text, SlackURL, %SlackURLText%
}

Gui, Show,, Bill.com Auto Syncer

;Main Code
CurrentName = ""

GoSub, MainLoop

#Persistent
SetTimer, MainLoop, %LoopTimerTime%
;1 hour = 3600000
return

Esc::ExitApp

Chosens:
	GuiControlGet, Choice,, SelectedItem
	IniWrite, %Choice%, billautosyncer.ini, Bill, delay
	global LoopTimerTime
	LoopTimerTime:=(Choice*60*1000)
return

SlackUrlChoose:
	GuiControlGet, SlackURLText,, SlackURL

	if (InStr(SlackURLText, "hooks.slack.com"))
	{
		global SlackUrl
		if (SlackURLText != SlackUrl)
		{
			;MsgBox Blah
			if (SlackTest(SlackURLText))
			{
				IniWrite, %SlackURLText%, billautosyncer.ini, Bill, SlackURLSetting
				SlackUrl = %SlackURLText%
			}
			else
			{
				MsgBox Slack URL is not valid please check the URL and update it
			}
		}
	}
	else
	{
		IniDelete, billautosyncer.ini, Bill, SlackURLSetting
		SlackUrl = %A_Space%
	}
return


FileExit:     ; User chose "Exit" from the File menu.
GuiClose:  ; User closed the window.
ExitApp

HelpAbout:
	Gui, 2:+owner1  ; Make the main window (Gui #1) the owner of the "about box" (Gui #2).
	Gui +Disabled  ; Disable main window.
	Gui, 2:Add, Text,, Code By Brandon Lis <brandon@artician.net>
	Gui, 2:Add, Text, gOpenBitBucket, Source code: https://bitbucket.org/knightar/billsync
	Gui, 2:Add, Button, Default, OK
	Gui, 2:Show
return

OpenBitBucket:
run, https://bitbucket.org/knightar/billsync
return

2ButtonOK:  ; This section is used by the "about box" above.
2GuiClose:
2GuiEscape:
	Gui, 1:-Disabled  ; Re-enable the main window (must be done prior to the next step).
	Gui Destroy  ; Destroy the about box.
return

StartBill:
	Run, BillComDashboard.exe, C:\Program Files (x86)\Bill.com\QBPlugIn2.0
	WinWait, ahk_pid %OutputVarPID%, , 3
	Sleep, 1
	WinActivate, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
return

SlackTest(SlackURL)
{
	if (InStr(SlackUrl, "hooks.slack.com"))
	{
		Json = {"text":"Slack test via Bill.com Auto Syncer, this means that the URL is valid."}
		WebRequest := ComObjCreate("WinHttp.WinHttpRequest.5.1")
		WebRequest.Open("POST", SlackURL)
		WebRequest.Send(Format(Json, CurrentName))
		if (WebRequest.ResponseText() = "Ok" && WebRequest.Status = 200)
		{
			return true
		}
	}
	return false
}

SlackNotice:
	global SlackURL
	if (InStr(SlackUrl, "hooks.slack.com"))
	{
		global CurrentName
		Json = {"text":"Bill.com Dashboard Failed to Sync! {1}"}

		WebRequest := ComObjCreate("WinHttp.WinHttpRequest.5.1")
		WebRequest.Open("POST", SlackURL)
		WebRequest.Send(Format(Json, CurrentName))
	}
return

EndProcess:
Loop {
	if !WinExist("ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1")
	{
		break
	}
	if WinExist("ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1")
	{
		Sleep, 1000
		WinClose, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
		WinWaitClose, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1, , 5
	}
}
return

MainLoop:
;WinGet, OutputVar, PID, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1

if WinExist("ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1")
{
	WinClose, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
	WinWaitClose, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1, , 5
}

if !WinExist("ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1")
{
	GoSub, StartBill
}
GoSub, SyncTargets
return

DetectNextAction:
Loop {
	if !WinExist("ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1")
	{
		break
	}

	ControlGetText, NoticeCheck, Static2, ahk_class #32770
	if (InStr(NoticeCheck, "The QuickBooks file is not available."))
	{
		ControlFocus, Static2, ahk_class #32770
		ControlGetPos, x, y, w, h, Static2, ahk_class #32770
		MouseMove, x, y
		ControlClick, OK, ahk_class #32770,,,, NA y1 x1
	}
	;A sync session is in progress. Are you sure?
	ControlGetText, StatusText, WindowsForms10.STATIC.app.0.1a0e24_r9_ad11, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
	if (InStr(StatusText, "Fix Your Sync"))
	{
		GoSub, SlackNotice
		WinMenuSelectItem ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1,, File, Close
		break
	}
	if (InStr(StatusText, "Shutting Down Connections..."))
	{
		; We'll have to handle this in the future
	}
	if (InStr(StatusText, "Close Window..."))
        {
		WinActivate, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
		ControlFocus, WindowsForms10.STATIC.app.0.1a0e24_r9_ad11, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
		ControlGetPos, x, y, w, h, WindowsForms10.STATIC.app.0.1a0e24_r9_ad11, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
		MouseMove, x, y
		ControlClick, Close Window..., ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1,,,, NA y1 x1
		break
	}
	ControlGetText, SyncButton, WindowsForms10.BUTTON.app.0.1a0e24_r9_ad16, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
	if (InStr(SyncButton, "Sync"))
	{
		break
	}
	Sleep, 10000
}
return

ForEachEntry:
ControlGet, SelectedList, List, Selected, WindowsForms10.LISTBOX.app.0.1a0e24_r9_ad11, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
Loop, Parse, SelectedList, `n
{
	global CurrentName
	CurrentName = %A_LoopField%

	WinActivate, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
	ControlFocus, WindowsForms10.BUTTON.app.0.1a0e24_r9_ad16, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
	Control, Choose, A_Index, WindowsForms10.LISTBOX.app.0.1a0e24_r9_ad11, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
	ControlClick, WindowsForms10.BUTTON.app.0.1a0e24_r9_ad16, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1,,,, NA x2 y2

	Gosub, DetectNextAction
}
return

SyncTargets:

ControlGetText, NoticeCheck, Static2, ahk_class #32770
if (InStr(NoticeCheck, "Syncasfas"))
{
	ControlFocus, Static2, ahk_class #32770
	ControlGetPos, x, y, w, h, Static2, ahk_class #32770
	MouseMove, x, y
	ControlClick, OK, ahk_class #32770,,,, NA y1 x1
	return
}

ControlGetText, NoticeCheck, Static2, ahk_class #32770
if (InStr(NoticeCheck, "The QuickBooks file is not available."))
{
	ControlFocus, Static2, ahk_class #32770
	ControlGetPos, x, y, w, h, Static2, ahk_class #32770
	MouseMove, x, y
	ControlClick, OK, ahk_class #32770,,,, NA y1 x1
	return
}

SetControlDelay -1
Gosub, DetectNextAction ; Make sure the window is back to normal first

global CurrentName
CurrentName = ""

ControlGetText, SyncButton, WindowsForms10.BUTTON.app.0.1a0e24_r9_ad16, ahk_class WindowsForms10.Window.8.app.0.1a0e24_r9_ad1
if (SyncButton)
{
	GoSub, ForEachEntry
	GoSub, EndProcess
}
else
{
	Gosub, DetectNextAction
}
return
